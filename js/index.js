$('.carousel').carousel({
    interval: 1000
  })
/* $(function () {
  $('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="popover"]').popover()
}) */
$('#add-game').on('show.bs.modal', function(e) {
  $('#forbidden').addClass('disabled')  
  $('#forbidden').removeClass('btn-primary')  
  $('#forbidden').removeClass('btn-add')  
  $('#forbidden').addClass('btn-warning')     
})

$('#add-game').on('hide.bs.modal', function(e) {
  $('#forbidden').removeClass('disabled')  
  $('#forbidden').addClass('btn-primary')  
  $('#forbidden').addClass('btn-add')  
  $('#forbidden').removeClass('btn-warning')     
})